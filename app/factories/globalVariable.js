/*
This component provides access to global functionalities and variables to avoid code duplication. 
*/
app.factory('globalVariable', function () {
	return {
		email: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
		category: {
			grocery: 'Grocery',
			departmental_stores: 'Departmental Store',
			stationary: 'Stationary',
			electronics: 'Electronics',
			aparel: 'Apparel',
			restaurants: 'Restaurants'
		},
		area: {
			tollygunge: 'Tollygunge',
			ajaynagar: 'Ajaynagar',
			golf_green: 'Golf Green',
			gariahat: 'Gariahat',
			garia: 'Garia'
		},
		city: {
			kolkata: 'Kolkata'
		},
		hideModal: function (selector) {
			$(selector).modal('hide')
		},
		showModal: function (selector) {
			$(selector).modal('show')
		}
	}
});