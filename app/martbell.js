app.config(
	['$stateProvider', '$urlRouterProvider',
	function ($stateProvider, $urlRouterProvider) {
			//
			// For any unmatched url, redirect to /state1
			// $urlRouterProvider.otherwise("/state1");
			//
			// Now set up the states
			$urlRouterProvider.otherwise('/');

			$stateProvider

			//////////
			// Home //
			//////////

				.state('home', {
				url: '/',
				views: {
					'': {
						controller: 'StorefrontCtrl',
						templateUrl: './app/partials/home.html'
					},
					'header': {
						controller: 'NavbarCtrl',
						templateUrl: 'app/partials/header.html',
					}
				}
			})

			////////////
			// Login //
			//////////

			.state('login', {
				url: '/login',
				views: {
					'': {
						controller: 'LoginCtrl',
						templateUrl: 'app/partials/login.html',
					},
					'header': {
						controller: 'NavbarCtrl',
						templateUrl: 'app/partials/header.html',
					}
				}
			})


			///////////////////
			// How it works //
			/////////////////

			.state('about', {
				url: '/about',
				views: {
					'': {
						templateUrl: 'app/partials/about.html',
					},
					'header': {
						controller: 'NavbarCtrl',
						templateUrl: 'app/partials/header.html',
					}
				}
			})

			///////////////////
			// Registration //
			/////////////////

			.state('registration', {
				url: '/registration',
				views: {
					'': {
						controller: 'RegistrationCtrl',
						templateUrl: 'app/partials/registration.html',
					},
					'header': {
						controller: 'NavbarCtrl',
						templateUrl: 'app/partials/header.html',
					}
				}
			})

			///////////////////
			// Menu         //
			/////////////////

			.state('menu', {
				url: '/products/:id/{name}',
				views: {
					'': {
						controller: 'MenuCtrl',
						templateUrl: 'app/partials/menu.html',
						resolve: {
							qId: function ($stateParams) {
								return $stateParams.id;
							}
						}
					},
					'header': {
						controller: 'NavbarCtrl',
						templateUrl: 'app/partials/header.html',
					}
				},


			});



}]);