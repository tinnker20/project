app.controller('ScrollCtrl', function($scope) {
  $scope.menuItems = [];

  $scope.loadMore = function() {
    var last = $scope.menuItems[$scope.menuItems.length - 1];
    for(var i = 1; i <= 8; i++) {
      $scope.menuItems.push(last + i);
    }
  };
});

    <div class="col-xs-6 col-sm-4 col-lg-9 row">
        <!-- Card Projects -->
      <div class="col-sm-4.5 col-md-3" ng-repeat="menuItems in storefront.menuItems | filter:search:strict | orderBy: order">
        <div class="thumbnail" >
          <h4 class="text-center"><span class="label label-info">{{menuItem.name}}</span></h4>
          <img ng-src="{{menuItem.photo}}" class="img-responsive">
          <div class="caption">
            <div class="row">
              <div class="col-md-6 col-xs-6">
                <h3>{{menuItem.name}}</h3>
              </div>
              <div class="col-md-6 col-xs-6 price">
                <h3>
                <label>{{menuItem.price}}</label></h3>
              </div>
            </div>
            <p>{{menuItem.description}}</p>
            <div class="row">
              <div class="col-md-6">
                <a href="#" class="btn btn-success btn-product"><span class="glyphicon glyphicon-shopping-cart"></span> Add</a></div>
            </div>

            <p> </p>
          </div>
        </div>
      </div>