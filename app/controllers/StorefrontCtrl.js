app.controller('StorefrontCtrl', ['$scope','storefront' ,'$http', 'globalVariable',
	function StorefrontCtrl($scope, storefront, $http, globalVariable) {

		$scope.search = {}
		$scope.CATEGORY_OPTIONS = globalVariable.category
		$scope.AREA_OPTIONS = globalVariable.area
		$scope.CITY_OPTIONS = globalVariable.city

		$scope.totalstoresDisplayed = 4;

		$scope.loadstoresMore = function () {
		  $scope.totalstoresDisplayed += 2;  
		};

		//watch 'search' property for build url filtering  
		$scope.$watch('search', function (search) {
			
			var storefronts = storefront.getAll();

			if (search.price && search.price!=null) {
				storefronts.equalTo('price',search.price)
			}
			if (search.category && search.category!=null) {
				storefronts.equalTo('category',search.category)
			}
			if (search.rating && search.rating!=null) {
				storefronts.equalTo('actions.ratings.avg',search.rating)
			}
			if (search.city && search.city!=null){
				storefronts.equalTo('city',search.city)
			}
			if(search.area && search.area!=null){
				storefronts.equalTo('area',search.area)
			}
			//get the storefront filtered
			storefronts.fetch().then(function(){
				$scope.$apply(function(){
					$scope.storefronts = storefronts.instance;
					$scope.order = 'name'
				})			
			},function(){
				$scope.error = 'Ops Something went wrong'
			})
	
		}, true)

		//function for added class on active element for sorting restaurant in table
		$scope.orderIs = function (order) {
			return order === $scope.order;
		}
}])